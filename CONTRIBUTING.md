# Contribuer au projet

## Guide pour les donateurs

Vous être libre de donner ce que vous voulez, à qui vous voulez, la où votre confiance et vos convictions vous mènent :
 
- Soutenir l'association [AGATAs](http://agatas.org/) et ses projets engagés dans la transitions écologique et sociale, pour lesquels elle finance le développement de Mycelia.
  - En donnant dès maintenant sur [helloasso](https://www.helloasso.com/associations/agatas/formulaires/1)
  - En [contactant l'association](mailto:contact@agatas.org) pour contractualiser autour d'un besoin spécifique (formation, assistance, animation événementielle, développement spécialisé...)
  
- Soutenir directement Millicent Billette, le développeur principal de Mycelia, dont vous pouvez suivre [l'activité globale](https://framagit.org/1000i100) ou [spécifique à mycelia-front-app](https://framagit.org/mycelia/mycelia-front-app/graphs/master)
  - en [monnaie libre](http://nayya.org/index.php/2017/08/06/une-monnaie-libre-existe/) (Ğ1) : [👤 : 1000i100 🔑 : 2sZF6j2P](https://g1.duniter.fr/#/app/wot/2sZF6j2PkxBDNAqUde7Dgo5x3crkerZpQ4rBqqJGn8QT/1000i100)
  - en monnaie dette (€) : [don récurant](https://liberapay.com/1000i100/) ou [don ponctuel](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=6JAUDKLT5RBVE)


## Guide pour les testeurs
1. [Créez un compte sur Framagit](https://framagit.org/users/sign_in#register-pane) *(si vous n'en avez pas déjà)*.
2. Recherchez dans [les issues](https://framagit.org/mycelia/mycelia-front-app/issues) si votre signalement de bug ou proposition n'a pas déjà été proposé (les tag ou labels peuvent vous aider à trouver).
   - s'il y a déjà des choses sur le sujet, vous pouvez soutenir la proposition existante et au besoin la compléter en commentaire.
   - si vous trouvez des doublons, vous pouvez les signaler en commentaire.
3. Créez une issue pour [signaler un bug](https://framagit.org/mycelia/mycelia-front-app/issues/new?issuable_template=bug) ou pour [proposer une évolution](https://framagit.org/mycelia/mycelia-front-app/issues/new?issuable_template=demande-compacte)
4. Validez. Merci pour votre contribution !


## Guide pour développeurs
1. [Créez un compte sur Framagit](https://framagit.org/users/sign_in#register-pane) *(si vous n'en avez pas déjà)*.
1. [Forkez le projet](https://framagit.org/mycelia/mycelia-front-app/forks/new) pour en avoir une version à votre nom *(si vous n'en avez pas déjà)*.
1. [Installez git](https://git-scm.com/) en version 2.3 minimum *(si pas déjà dispo sur l'ordinateur)*.
1. [Installez nodejs](https://nodejs.org/fr/) (v8 ou plus) avec npm *(si pas déjà dispo sur l'ordinateur)*.
1. Créez votre clef ssh *(si vous n'en avez pas déjà sur cet ordinateur)*
   - Dans un terminal (ou git-bash pour les utilisateurs Windows) tapez :
   - `ssh-keygen -t rsa -C "nom utilisateur et nom machine ex: Millicent PC portable"` puis faire entrer à chaque question sans rien mettre.
1. Autorisez votre clef ssh pour votre compte Framagit *(si ce n'est pas déjà fait)* :
   - Dans un terminal (ou git-bash pour les utilisateurs Windows) tapez :
   - `cat ~/.ssh/id_rsa.pub` copier les quelques lignes qui s'affiche (de `ssh-rsa` jusqu'au nom que vous avez donné à la clef.)
   - Dans la [page de gestion des clef ssh de Framagit](https://framagit.org/profile/keys) collez la clef dans la zone `key` puis valider en appuyant sur `add key`.
1. Placez-vous dans le dossier ou vous souhaiter créer une copie locale du projet pour la modifier.
1. Ouvrez un terminal dans ce dossier (clic droit, git-bash here pour les utilisateurs Windows)
1. Tapez `git clone git@framagit.org:votreFork/mycelia-front-app.git .` (attention modifiez cette commande en remplaçant `votreFork` par le nom de votre compte sur Framagit)
1. Tapez `git remote add official https://framagit.org/mycelia/mycelia-front-app.git` pour pouvoir mettre à jour votre version a partir des modifications du dépot officiel.
1. Lancer `npm install` pour charger les dépendances du projet.
1. Effectuer vos contributions.
1. Tester que ce qui fonctionnait continue de fonctionner avec la commande `npm run test` puis en copiant l'url proposé dans un navigateur moderne (ou plusieurs).
1. Si tous va bien (et si possible que vous avez ajouté de nouveau test pour couvrir vos contributions) vous pouvez effectuer un commit
   - `git rm -rf --cached .` supprime ce qui à été supprimé (et le reste) de ce qui est à sauvegarder
   - `git add .` ajoute tout ce qui est présent dans le projet (sauf les exeptions listé dans le fichier `.gitignore`)
   - `git status` pour vérifier que ce que vous allez sauvagarder est bien ce que vous souhaitez.
   - `git diff nomDuFichier` si vous avez un doute sur ce qui à changé par rapport à la version précédant vos modifications.
   - `git commit -m "FIX: ce que vous avez corrigé/ajouté"` sauvegarde votre correctif avec son descriptif
   - ou `git commit -m "FEAT: votre nouvelle fonctionnalité"` sauvegarde votre nouvelle fonctionnalité avec son descriptif
   - `git pull official` charge les nouveauté du dépot officiel s'il y en a.
   - Vérifiez que tout fonctionne toujours avec `npm run test` puis en copiant l'url proposé dans un navigateur moderne (ou plusieurs).
     - Corrigez si nécessaire et refaite un commit.
   - Enfin `git push` publiez sur votre dépots vos contributions.
   - Sur framagit proposez une [merge-request](https://framagit.org/mycelia/mycelia-front-app/merge_requests/new)
   - Validez. Merci pour votre contribution !


## Guide pour traducteurs
1. [Créez un compte sur Framagit](https://framagit.org/users/sign_in#register-pane) *(si vous n'en avez pas déjà)*.
1. [Forkez le projet](https://framagit.org/mycelia/mycelia-front-app/forks/new) pour en avoir une version à votre nom *(si vous n'en avez pas déjà)*.
1. Modifiez le fichier `staticApp/lang/fr.yml` (ou équivalent dans votre langue) depuis l'interface de gitlab ou en le clonant le projet localement (comme pour les développeurs).
1. Sauvegardez/commit/push merge-request vos modifications (comme pour les développeurs).



