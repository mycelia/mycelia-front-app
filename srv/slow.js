#!/usr/bin/env node
const speed = 1;

const path = process.cwd();
const nStatic = require('node-static');
const fileServer = new nStatic.Server(path);
const http = require('http');

const start = Date.now();
const fifo = [];
const srv = http.createServer((request, response)=>
	request.addListener('end', ()=>{
		fifo.push({request,response});
	} ).resume()
);
setInterval(slowllyAnswer,1000/speed);
function slowllyAnswer(){
	if(fifo.length === 0) return;
	const {request, response} = fifo.shift();
	fileServer.serve(request, response);
	const now = Date.now();
	const elapsed = Math.round((now - start)/1000)/1;
	console.log(`(${fifo.length}) After ${elapsed}s answering : ${request.url}`);
}
srv.listen(process.argv[2], ()=> {
	console.log('Mycelia front-end is now locally available here : http://localhost:'+srv.address().port+'/');
	require("opn")("http://localhost:"+srv.address().port+"/");
});
