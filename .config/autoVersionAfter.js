#!/usr/bin/env node
const fs = require('fs');
const git = require('simple-git/promise');

finalizeCommit();
async function finalizeCommit() {
	fs.readdirSync(`${process.cwd()}/.git/`);
	const amendNeed = fs.readdirSync(`${process.cwd()}/.git/`);
	if(amendNeed.indexOf('needAmend') !== -1){
		const gitRepos = git(process.cwd());
		await gitRepos.add(['package.json', 'CHANGELOG.md']);
		fs.unlinkSync(`${process.cwd()}/.git/needAmend`);
		await gitRepos.raw('commit --amend -C HEAD --no-verify'.split(' '));
		// addTag
		const pkg = JSON.parse(fs.readFileSync(`${process.cwd()}/package.json`, 'utf8'));
		await git(process.cwd()).addTag(pkg.version);

	}
}
