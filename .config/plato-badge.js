const execSync = require('child_process').execSync;

function badgeColor(percent) {
	let color = 'red';
	if(percent>25) color = 'orange';
	if(percent>50) color = 'yellow';
	if(percent>70) color = 'yellowgreen';
	if(percent>80) color = 'green';
	if(percent>90) color = 'brightgreen';
	return color;
}

const report = require('../plato/report.json');
const maintainabilityScore = Math.round(parseFloat(report.summary.average.maintainability));
let maintainabilityColor = badgeColor(maintainabilityScore);

const sloc = report.summary.total.sloc;
execSync('wget -q -O plato/maintainability.svg https://img.shields.io/badge/maintainability-'+maintainabilityScore+'-'+maintainabilityColor+'.svg');
execSync('wget -q -O plato/total-sloc.svg "https://img.shields.io/badge/total sloc-'+sloc+'-blue.svg"');
