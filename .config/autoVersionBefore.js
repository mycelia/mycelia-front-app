#!/usr/bin/env node
const fs = require('fs');
const git = require('simple-git/promise');

smartCommit();
async function smartCommit() {
	fs.writeFileSync(`${process.cwd()}/.git/needAmend`, 'true');

	const commit = {};
	commit.msg = fs.readFileSync(`${process.cwd()}/.git/COMMIT_EDITMSG`, 'utf8');
	commit.type = commitVersionType(commit.msg);
	commit.author = await authorInfo();
	commit.version = await updatePackageJsonVersion(commit.type);
	await updateChangelog(commit);

	try{
		await git(process.cwd()).raw(['push', '--tags']); // push pendings tags
	} catch (err){
		console.error('git push --tags fail :',err);
	}
}
async function updatePackageJsonVersion(type){
	const pkg = JSON.parse(fs.readFileSync(`${process.cwd()}/package.json`, 'utf8'));
	if(type){
		pkg.version = bumpVersion(pkg.version,type);
	}
	fs.writeFileSync(`${process.cwd()}/package.json`, JSON.stringify(pkg,null,2));
	return pkg.version;
}
async function updateChangelog(commit){
	let orgChangelog;
	try{
		orgChangelog = fs.readFileSync(`${process.cwd()}/CHANGELOG.md`, 'utf8');
	} catch (e){
		orgChangelog = '';
	}
	const newChangeLog = updateChangelogText(orgChangelog,commit);
	fs.writeFileSync(`${process.cwd()}/CHANGELOG.md`, newChangeLog);
}
function bumpVersion(originalVersion,bumpType){
	const versionArray = originalVersion.split('.');
	if(bumpType === 'minor'){
		versionArray[1]++;
		versionArray[2] = 0;
	}
	if(bumpType === 'patch') versionArray[2]++;
	return versionArray.join('.');
}
function commitVersionType(commitMsg){
	let commitType = '';
	if(commitMsg.search(/FEAT*:/i) !== -1) commitType = 'minor';
	else if(commitMsg.search(/FIX*:/i) !== -1) commitType = 'patch';
	return commitType;
}
function updateChangelogText(oldText,commit){
	const now = new Date();
	let titleLine = `*👤 [${commit.author.shortName}](mailto:${commit.author.email}) ⏰ ${now.toISOString().substr(0,now.toISOString().lastIndexOf(':')).replace('T',' ')}*`;
	if(commit.type==="patch") titleLine = `### v${commit.version} \n${titleLine}`;
	if(commit.type==="minor") titleLine = `## Version ${commit.version} \n${titleLine}`;
	return `${titleLine}

${commit.msg.split("\n").filter(l => l.indexOf(':') !== -1).map(l => `    ${l.trim()}`).join("\n")}

${oldText}`;
}
async function authorInfo(){
	const author = {};
	author.fullName = (await git(process.cwd()).raw(['config', 'user.name'])).trim();
	author.email = (await git(process.cwd()).raw(['config', 'user.email'])).trim();
	author.shortName = author.fullName.split('/')[0].split(' - ')[0].split(']')[0].trim();
	if(author.shortName[0]==='[')author.shortName = author.shortName.substring(1).trim();
	return author;
}
