### v0.19.6 
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2019-05-13 14:41*

    FIX: update dependencies

### v0.19.5 
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2018-06-11 08:13*

    FIX: Licence badge build

### v0.19.4 
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2018-06-11 08:06*

    FIX: Don't crash on empty template

### v0.19.3 
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2018-06-11 06:18*

    FIX: Auto-retry on fail test TODO: more deterministic async test

### v0.19.2 
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2018-06-11 06:16*

    FIX: Auto-retry on fail test TODO: more deterministic async test

### v0.19.1 
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2018-06-11 06:01*

    FIX: Dependency updates

## Version 0.19.0 
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2018-06-11 05:07*

    FEAT: changement de licence AGPL-3.0 -> EUPL-1.2 à compatibilité modifiée.

### v0.18.1 
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2018-06-06 00:57*

    FIX: build downloadable static package
    DOC: badge for version/release, download and liberapay

## Version 0.18.0 
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2018-06-06 00:00*

    FEAT: nearlyHidden hide name for better graph read (and perf)
    FEAT: LoD perf tweak -> nearlyHidden hide arrow if LOD < 8

### v0.17.6 
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2018-04-05 11:29*

    FIX: patch some test to avoid conflict

### v0.17.5 
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2018-04-05 10:36*

    FIX: refacto massive dependencies updates including d3js V5 * without merge regression

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2018-04-05 10:02*

    # Conflicts:

### v0.17.4 
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2018-04-05 10:01*

    FIX: refacto massive dependencies updates including d3js V5
### v0.17.3 
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-12-23 00:43*

    FIX: plato-historic now run one by month and no more one by day.

### v0.17.2 
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-12-23 00:33*

    FIX: update CI triggers and fail conditions

### v0.17.1 
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-12-23 00:22*

    FIX: update es6-plato CI step

## Version 0.17.0 
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-12-23 00:02*

    FEAT: auto-push old git tags

### v0.16.12 
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-12-22 23:52*

    FIX: correctly upate cache in localhost with serviceworker

### v0.16.11 
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-12-22 22:46*

    FIX: issue #145 hovering a node after link addition now highlight all neighbors including new-ones.

### v0.16.10 
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-12-22 22:14*

    FIX: auto git tag version now target the good version.

### v0.16.9 
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-12-22 21:44*

    FIX: no more mycelia crash because of dead links (node missing as link source or target)

### v0.16.8 
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-12-22 21:15*

    FIX: no more localstorage conflict between instances (when they share same domain but different path).

### v0.16.7 
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-12-12 04:43*

    FIX: gitlab-ci need more commented lines to run without coverage...

### v0.16.6 
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-12-12 04:11*

    FIX: build.stylus adjust to match the default/theme/style.styl refacto

### v0.16.5 
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-12-12 04:04*

    BAD: drop karma-coverage (no async/await support) without replacing it with jest-coverage.
    FIX: better implementation for ymlTools loadMerge to fix occasional interference between config files and trad files
    REFACTO: ymlTools switch to async/await style with legacy event spaming behaviour.

### v0.16.4 
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-12-12 01:22*

    FIX: default font path was not include in build
    FIX: graph load order fix to prevent simulation error message.
    REFACTO: default anything moved to staticApp/default

### v0.16.3 
*👤 [Ninon](mailto:ninon.n@agatas.org) ⏰ 2017-12-11 14:57*

    FIX: [trad] Licence title
    FIX: [style] picto pages

### v0.16.2 
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-12-03 04:53*

    FIX: demo customStyle.styl now use stylus function/mixin to avoid some duplication

### v0.16.1 
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-12-01 03:46*

    FIX: auto-version don't add git tag when there is non version bump

## Version 0.16.0 
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-12-01 03:43*

    FEAT: auto-version add git tag

### v0.15.7 
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-12-01 02:04*

    FIX: auto-changelog with clickable author -> email

### v0.15.6 
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-12-01 01:58*

    FIX: better auto-generating of futur changelog additions
    DOC: Full changelog
    DOC: changelog and roadmap in README.md

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-11-30 11:35*

    REFACTO: cleaning auto changelog

### v0.15.5 
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-11-30 11:32*

    FIX: nice date formating for changelog

### v0.15.4
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-11-30 11:10*

    FIX: Legacy date formating for changelog

## Version 0.15.0
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-11-30 10:00*

    FEAT: autofill changelog with commit message (tag:workflow)
    FIX: better SW caching (stop making 10 times the job).
    FIX: no cache.json update delay fix.
    FIX: no more silly updateCache call.
    FIX: service worker pretty well optimised now.
    FIX: skipWaiting for loading new service worker.
    FIX: fold/unfold arrow change for better lisibility.
    DOC: Contributing guide (fr)
    DOC: new smaller gitlab templates.

## Version 0.14.0
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-11-28 16:26*

    FEAT: service worker cache working... a bit.
    FIX: fluid switch between pages and viz now working.
    FIX: change + - by direction arrow

## Version 0.12.0
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-11-23 03:07*

    FEAT: basic LoD (Level of Details) management
    FIX: better langPicker placement

## Version 0.11.0
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-11-22 22:49*

    FEAT: display avg, min and max framerate

*👤 [Michael Scherer](mailto:misc@redhat.com) ⏰ 2017-11-19 11:02*

    FIX: typo error on "disponible"

### v0.10.5
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-11-17 11:55*

    FIX: traduction du résumé de la licence AGPLv3 terminé.

### v0.10.1
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-11-15 21:35*

    FIX: transition aux survole désormais plus fluides.

## Version 0.10.0
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-11-15 20:33*

    FEAT: AGPLv3 pointe vers une description de la licence
    WIP: traduction du résumé de la licence

## Version 0.9.0
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-11-15 12:08*

    FEAT: handle advenced math in autoCalc form field

### v0.8.6
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-11-02 20:30*

    FIX: display email error when there occurs.
    FIX: default theme font in place.

### v0.8.3
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-10-16 08:22*

    FIX: npmignore don't block local file: include anymore.

### v0.8.2
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-10-10 10:37*

    FIX: mentions légales avec moins de fautes

## Version 0.8.0
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-10-07 03:17*

    FEAT: [multi-user] saveButton first functionnal version !!!
    FEAT: ev.need promise mode (handle await syntax now)
    FIX: [multi-user] saveButton clear localstorage after successful save.
    FIX: fix layer error message, but not why layers go back to undefined when there are an empty array.

## Version 0.6.0
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-10-05 00:58*

    FEAT: [multi-user] when logged-in say hello and display your rights in advToolsPanel
    FEAT: [multi-user] you can logout when you're logged-in.
    FIX: logged ev broadcast now broadcast

### v0.5.11
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-10-02 01:02*

    FIX: trad with legals fr title
    FIX: legals styl is now readable

## Version 0.5.0
*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-09-29 09:40*

    FEAT: build a staticPackage with pageLoading optimisation and size optimisation (more to come).
    FEAT: autoPublish to npm with smart auto version update via CI.
    FIX: README.md is now up-to-date !
    FIX: remove now useless dependencies
    FIX: remove some dead-code
    FIX: CI staticPackage archive content is now in a more semantic subfolder
    FIX: autoVersion speed optimisation avoiding strang loop.
    FIX: autoVersion split before and after for better version handling.
    FIX: include the modified version number in current commit.
    FIX: update outdated devDependencies
    FIX: update outdated dependencies

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-09-28 03:49*

    FEAT: change url hash trigger the appropriate config change... when it come from inside the page.

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-09-26 09:43*

    FEAT: pageView can process markdown template to display them in the details panel.
    FEAT: defaults legals are displayed.

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-09-24 23:43*

    FIX: trad save link now with good cursor pointer.

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-09-19 08:47*

    FEAT: faster lighter mini server

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-09-01 08:55*

    FEAT: graph picto from layers now configurable in config.yml
    FIX: la zone du logo ne déborde plus.
    FIX: default ui picto now in static app. (and css is updated)

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-08-31 18:07*

    FEAT: local micro server now speak english when it start.
    FIX: persistanceAPI don't end with a slash

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-08-29 08:07*

    FEAT: mycelia-front-srv callable by cli with port parameter
    FIX: mycelia-front-srv cli working
    WIP: login to edit-server nearer to be functionnal.

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-08-24 02:44*

    FIX: picto and legend size according to @GaetanG dirty commit

*👤 [GaetanG](mailto:guyet.gaetan@hotmail.fr) ⏰ 2017-08-18 10:43*

    FIX: [demo.mycelia.tools] modif lang
    FIX: [demo.mycelia.tools] modif config

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-08-15 01:25*

    WiP: advTools will send cors request well... a day...

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-08-11 01:49*

    FIX: advTools fetch with cookies up-to-date method

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-07-21 17:46*

    FEAT: add advTools sending auth-email when config persistanceAPI is defined
    FIX: publicData -> data.yml for private case homogen naming

*👤 [Elisa](mailto:elisa.bergeret@gmail.com) ⏰ 2017-06-14 13:15*

    FIX: ymlTools.js remplacement fetch par xmlhttprequest

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-06-13 20:23*

    FIX: graph loading bug (il y avait besoin de déplier/replier un paneau pour avoir l'affichage correcte)

*👤 [Elisa](mailto:elisa.bergeret@gmail.com) ⏰ 2017-06-09 13:07*

    FEAT: gitlab-ci : coverage

*👤 [Elisa](mailto:elisa.bergeret@gmail.com) ⏰ 2017-06-08 15:14*

    FIX: corrections orthographiques

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-06-07 23:49*

    FEAT: logo de l'élément dans la notice ainsi que son label (fait à l'arrache avec du !important dans le css pour écraser le customCss. A corriger donc)

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-05-24 12:14*

    FIX: no more external CDN dependency

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-05-15 18:29*

    FIX: traduction blocante en moins.
    FIX: traduction manquante qui ne manque plus.

*👤 [GaetanG](mailto:guyet.gaetan@hotmail.fr) ⏰ 2017-05-15 15:42*

    FIX: [style] modif épaisseur lien+ajout picto dans légende
    DOC: ajout gabarit fonctionnalité

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-05-15 07:37*

    FEAT: history renseigne le nom dès le chargement de la page et le change correctement.
    FIX: affichage masquage des calques fonctionnel, vraiement !
    FIX: amélioration conséquente des performances lié à la gestion des calques. #102
    FIX: séparation fullGraph -> filteredGraph -> displayedGraph enfin hermétique
    FIX: graph.js ligne 402 -> 429 les noeud figé se défigent désormais correctement
    FIX: panneau détails déployable dès le chargement
    FIX: correction de la marge basse des formulaire et trad.
    FIX: correctif pour le chargement de l'éditeur de trad
    FIX: trad fiable (mais le changement de langue nécessite désormais un F5)
    REFACTO: dead code cleaning

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-05-14 23:20*

    FEAT: amélioration des performance d'affichage des icones dans le graph selon les calques.
    FEAT: class userMode dans le header du panneau de droite pour pouvoir changer l'icone selon le mode.
    FEAT: activation du module d'historique
    FEAT: meilleur gestion du title dans le module historique (reste largement perfectible)
    FEAT: smartEvent -> whenInDom pour insérer selon un sélecteur qui n'est pas encore valide.
    FIX: couverture de test plus réaliste (suppression des dummy test)
    FIX: ajustement du chargement des modules de traduction pour pouvoir les charger plus tôt et ne plus avoir les souci de traduction appliqué qu'a moitiée.
    FIX: le titre est désormais chargé via le module de traduction (et donc traductible).

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-05-11 06:55*

    FEAT: [workflow] plato-report
    FEAT: [workflow] maintainability badge
    FIX: require report.json path
    FIX: AGPLv3 badge
    FIX: AGPLv3 en bas de page sur la démo
    FIX: lint stringTools
    FIX: CI stages cleaner
    FIX: plato-historic CI & lighting tests

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-05-10 20:37*

    FEAT: [workflow] gitlab-ci avec test, fonctionnel !
    FEAT: [workflow] demo and coverage auto-publish
    FEAT: [workflow] better than cash, fresh dedicated docker-images
    FEAT: [workflow] gitlab-ci avec cache
    FEAT: dataType hidden fonctionnel et testé
    FIX: better karma reporting
    FIX: update readme.md badges

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-05-5 18:42*

    FEAT: #83 affichage des voisins au survol (version poudre au yeux)
    FEAT: [workflow] publication automatique sur demo.mycelia.tools
    FIX: re-titrage : Demo Mycelia
    FIX: style ancien liens
    WIP: #83 chaques voisins en tant que class

*👤 [GaetanG](mailto:guyet.gaetan@hotmail.fr) ⏰ 2017-05-05 15:30*

    FIX: config liens passés
    FIX: config.styl

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-05-5 14:48*

    FIX: flèche droites
    FIX: flèche de la même couleur que leur lien
    FIX: flèche retravaillée

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-05-4 23:37*

    FEAT: première version liens directionnels "propre"
    WIP: flèches basique, indépendante de la taille de la cible.

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-04-21 16:51*

    FIX: mise à jour package.json pour faire un npm publish avec un nom a base de mycelia

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-04-10 17:48*

    FIX: pas de log ou fichier d'erreurs à versionner dans le code.

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-03-18 18:53*

    FIX: mise à jour du mini serveur de test pour éviter les conflit en cas de multiple lancement.
    FIX: dépendance phantomjs obsolete

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-03-10 03:46*

    FIX: ajustement de style du graph. Zone sélectionnable en particulier.

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-03-4 12:46*

    FIX: avertissement version navigateur aussi pour les vieilles version de firefox

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-03-2 01:40*

    FEAT: couverture de test effective
    FIX: [typo] picto, trad, orthographe, accents...
    FIX: karma running
    FIX: trad moins brutale

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-02-28 12:00*

    FEAT: ajout d'un mini serveur pour fichier statiques lançable avec npm start.

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-02-24 19:58*

    FEAT: ajustement timeout requirejs et ajout de failOver pour charger les lib à distance ou en local selon dispo
    FEAT: des liens qui réagissent au survol et qu'on peut déselectionner aussi !
    FEAT: différenciation visuelle au survole des calques: OK !
    FEAT: les élément du graph ont les class des calques dont ils font parti.
    FEAT: le graph affiche les icon correspondant au calque Fonction correspondant.
    FEAT: masquage des calques vides
    FEAT: affichage sommaire de la notice
    FEAT: add vizMode button
    FEAT: anim de chargement pour patienter
    FIX: anim de chargement moins agressive
    FIX: affichage noscript propre
    FIX: trad gen
    FIX: bug visuel menu légende
    FIX: [style] mise en page global et notice en particulier.
    FIX: [style] petit ajustement design champs de recherche
    FIX: [style] ajustement couleurs liens
    FIX: [style] font Ubuntu
    FIX: [style] pictos
    WIP: les ajout/suppression de noeud bug toujours, mais c'est plus discret.

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-02-23 20:11*

    FEAT: exportFile now work on proxyfied struct
    FEAT: legendView branché sur le graph pour l'affichage du nombre d'entitées inclues
    FIX: toInit dummy value to avoid conflict
    FIX: [style] ui calques
    FIX: corrections des xpath dans config.yml
    FIX: plus de double titre dans la colonne légende

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-02-23 12:51*

    FIX: smartEvents passe de nouveau par les event natif du js, plutôt pour le meilleurs, vu l'usage qui en est fait en tout cas.
    WIP: smartEvents ne passe plus par les event natif du js... pour le meilleurs ou pour le pire... on va voir ça.

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-02-22 15:53*

    FEAT: calques arborescents intégré et fonctionnels quand ça leur chante
    FEAT: buildNode handle cssStyle id and classes
    REFACTO: semantic reffacto subCriteria -> sub
    WIP: cablage de filtre, restructuration, code non testé... explosion en approche.

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-02-20 13:18*

    FEAT: configParser du layer engine fonctionnel et intégré.
    FEAT: configLoader ready for parserPlugins
    FEAT: smartEvents gère désormais les listener ordonnés.
    FEAT: xpath recursif pour préparer l'arrivée des calques, et filtres new gen
    FIX: configParser event trigger.

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-02-19 09:57*

    FEAT: [graph-viz] lien sélectionnable (et plus épais et avec indication visuelle de sélection)
    FIX: [form] préfix avec _ plutôt que . pour éviter les conflit avec les évènements composé et les classes css.
    FIX: autoCalc de nouveau à la saisie et pas uniquement au changement de champ.
    FIX: plus de bug de perméabilité entre fullGraph et currentGraph.
    FIX: style et autoCalc
    WIP: préparation des calques nouvelles formule dans la config

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-02-18 20:40*

    FEAT: champs de titre implémenté pour les formulaires.

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-02-16 12:33*

    FEAT: [style] ajout d'un favicon

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-02-16 07:04*

    FEAT: [workflow] insertion d'étapes de contrôle qualité.
    FIX: form avec autoCalc numérique et gestion de l'écrasement correcte.
    FIX: limit id size in form.js
    REFACTO: duplication extermination
    DOC: gabarit gitlab pour faciliter la saisie de demandes précises, pertinentes et réalisable.

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-02-14 04:29*

    SWITCH: drop conditionEvaluator for xpath based conditions evaluation.
    + more versatile
    - harder to understand for form.yml creator

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-02-13 06:07*

    FIX: yaml exception in chrome
    FIX: [style] design interface globale & ergonomie formulaire

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-02-11 09:31*

    FIX: update dependencies
    FIX: inline yml with newline to prevent chrome error in yml parsing.
    FIX: mock sans vomir dans la prod.
    REFACTO: less duplication, less complexity, more coverage
    WIP: prépa build
    WIP: xpath ready (with no infinite recursion)
    DOC: diagrame de fonctionnement
    TODO: jspm plutôt que requirejs

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-02-08 05:05*

    FIX: form data insertion

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-02-07 12:11*

    FEAT: mini lib pour convertir des json en objet dom reconvertible à l'identique en json.

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-02-04 00:12*

    REFACTO: changeInputValue now in smartEvents and used for autoCalc field to send event on change.

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-02-02 03:21*

    FEAT: autoCalc sommaire pour les champs de formulaire (concaténation de chaine de caractère, pas de formule mathématique).
    FIX: fichier langue nomé avec la langue en cours.
    REFACTO: move all trad module to trad folder and adjust links

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-02-01 04:51*

    FIX: tradEditor mieux couvert par des test (mais pas encore complètement)

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-01-29 19:00*

    FIX : trad load-in sequence

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-01-28 00:02*

    FIX: monitoredStruct more robust

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-01-26 22:44*

    FIX: existing test
    REFACTO: gros refacto incluant la suppression de l'ancien languageLoader
    WIP: laisse des truc moche dans graph et des truc pas testé dans formLoader et tradEditor avec des suspition de bug restant à corriger.

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-01-25 10:57*

    FEAT: console log généraliste dans main.js
    SWITCH: bascule de langLoader au module trad
    FIX: [style] un peu de mise en page
    FIX: complément de traduction

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-01-24 23:38*

    FEAT: ajout d'un module trad pour chapoter les sous modules

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-01-24 01:26*

    FIX: multiLoad couvert par un test
    REFACTO: for of plutôt que for in quand c'est possible pour une meilleure lisibilité

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-01-16 00:32*

    REFACTO: extraction du comportement d'historisation du module Store vers un module dédié.

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-01-11 21:37*

    FEAT: tradRenderer testé et fonctionnel.
    REFACTO: tradRenderer et test associé (réduction complexité et duplication)

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-01-09 20:50*

    REFACTO: form.js

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2017-01-08 04:25*

    FIX: getValidLang tested
    FIX: opérandes testCases are now really executed.
    FIX: better Store test and proper deletion handling
    FIX: more proper diff than removeDefault and fix deletion from ref state to notice deletion.
    FIX: empty and false-like structManipulation useCases
    FIX: config loader tested
    REFACTO: duplication reduction
    REFACTO: extract function for complexity reduction

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2016-12-29 15:16*

    REFACTO: supprimer d'un max de duplication dans test conditionEvaluator 
    REFACTO: extraction de la logique des conditions du module form.

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2016-12-28 12:33*

    FEAT: tradChooser prêt.
    FIX: données magiques transféré dans la config.
    REFACTO: déplacement du langBrutalSwitch vers tradRenderer
    WIP: brutalSwitch non appellé (à remplacer/corriger)

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2016-12-22 06:46*

    FIX: fps is now really tested
    REFACTO: reduce duplicacy and improve unit in htmlTools test 
    REFACTO: htmlTools fragment test to make them more unit

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2016-12-20 00:20*

    REFACTO: htmlBuilder -> htmlTools
    REFACTO: inclusion de applySelectiveClassOnNodes dans htmlTools plutôt que dans tradChooser.
    TODO: remplacer applySelectiveClassOnNodes par plusieurs petites fonction éventuellement composé pour redonner le même résultat.

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2016-12-19 22:46*

    FEAT: smartEvents record all listeners and can destroy them all by reset (for independant test usecase manly)
    FEAT: give is now destroyable.
    FIX: tradLoader and associated conf

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2016-12-18 07:15*

    FEAT: smartEvents give to answer need
    FIX: tradLoader n'a plus de constantes magique (mes les chargent depuis la config).
    FIX: tradLoader n'expose plus setTradPath

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2016-12-15 06:06*

    FEAT: [internal] htmlBuilder -> addOrReplace & addOnce
    FEAT: smartEvents need for unordered init stuff
    REFACTO: htmlBuilder extraction

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2016-12-13 22:38*

    FIX: test config & ymlTools loadFile test.
    WIP: trad beginining big refacto

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2016-12-05 06:43*

    FIX: monkey fix travis coverage codeclimate
    DOC: début de documentation formulaire

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2016-11-30 04:16*

    BAD: travis update with token for codeclimate coverage
    FIX: rustine pour config vide

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2016-11-29 18:51*

    FEAT: affichage de message d'erreur avec liens vers tutoriel
    FIX: chargement de l'application y compris sans allData.
    REFACTO: netoyage de l'interface
    DOC: mini mise à jour du readme

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2016-11-29 07:13*

    FEAT: export des données saisies en yml
    FEAT: [form] test and handling numeric operator in if form entry
    FEAT: [form] if with advenced conditions
    FEAT: [form] if with basic condition opperationnal
    FIX: Suppression des event "change" lorsqu'on attribut la même valeur au même endroit.
    REFACTO: form.js
    WIP: gestion chaotique de la mise à jour du graph
    WIP: test au rouge des conditions faisant référence à d'autres entitées

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2016-11-26 05:30*

    FEAT: [workflow] coverage reports addition
    FEAT: [workflow] adding travis-CI auto-buid/test & coverall coverage report
    FIX: Using browsers last version in CI
    FIX: réagencement des badges
    REFACTO: duplication and complexity reduction
    DOC: more metrics

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2016-11-25 17:56*

    FIX: duplication reduction (plus particulièrement dans les test)

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2016-11-25 07:56*

    FEAT: [form] charge les données existante quand il y en a
    FEAT: [form] sauvegarde les données saisies dès qu'un id est défini
    REFACTO: tests

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2016-11-24 21:17*

    FEAT: [form] génération d'identifiant au moment adéquat.
    FEAT: [internal] smartEvents.js -> gestion des listener à fragments sans ordre défini.
    FEAT: [edit] stringTools -> suppression des accents et remplacement des cractères spéciaux par des caractères compatible pour un ID xml ou un path d'url.

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2016-11-23 15:58*

    FEAT: [internal] smartEvents.js -> add 'after' group event catcher
    SWITCH: graphData to yml
    FIX: too generic event incorrectly trigger listener in smartEvents lib

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2016-11-23 02:01*

    FEAT: [form] handle static and dynamic list
    FEAT: [internal] add clickOn in event.js to trigger click on element
    FEAT: add exportAsFile in ymlTools
    FEAT: load graphData form yml and local and save it in local.
    FIX: switch structured event part order to have name before action type in MonitoredStruct
    REFACTO: add generic Store in place of urlHashStore and localStore

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2016-11-22 17:26*

    FEAT: switch to editor mode functionnal
    WIP: form basics

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2016-11-16 11:26*

    REFACTO: passage des modules en amd pur (requirejs) histoire d'éliminer le code mort.
    WIP: première briques pour le formulaire

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2016-11-15 17:37*

    FEAT: [workflow] karma actif partout et le premier vrai coverage... à 23%
    FEAT: [workflow] gh-pages incluera désormais les rapports de couverture de code.
    FIX: coverage qui pointe au bon endroit.
    REFACTO: coverage dans un dossier classique plutôt que caché
    DOC: coverageBadge

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2016-11-15 01:36*

    WIP: jest est simple, mais ces test sont faillible.
    WIP: commit avant switch

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2016-11-11 06:39*

    FEAT: [internal] la lib de gestion des évènement gère désormais les évènement structuré/fragmenté/modulaire
    FEAT: [internal] MonitoredStruct envoi des évènements au moindre changement d'une structure choisie.

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2016-11-9 07:45*

    FIX: conversion des chemin environnement node en cdn pour gh-pages
    REFACTO: modularisation

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2016-11-8 21:55*

    FEAT: masquage partiel des noeud au survole des calques/layers

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2016-11-8 10:54*

    FEAT: [internal] requirejs pour l'inclusion des fichiers js.
    REFACTO: préparation pour requirejs

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2016-11-7 16:46*

    REFACTO: event et panels

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2016-10-27 21:22*

    FEAT: [trad] ajout d'un formulaire de traduction pour les expression dynamiques, et téléchargement du fichier correspondant.

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2016-10-27 09:42*

    FEAT: [trad] v1 multilingue
    FEAT: [trad] prise en charge de la traduction pour toutes les données affichées dynamiquement (sans gestions des plurals)
    SWITCH: changement du format des fichiers de config : json -> yml
    DOC: pour l'UI statique, traduction possible mais les chaine à traduire ne sont pas détecté automatiquement.
    FIXME: dans l'implémentation actuelle, risque de bug si des mots clefs html ou js présent dans la page sont à traduire dans certains contexte et pas dans d'autres.
    REFACTO: loader

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2016-10-25 15:39*

    FEAT: Gestion de la selection et de la zone details

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2016-10-23 21:52*

    FIX: des nombres magiques en moins, des images d'image inconnu en plus, quelques bugfix sur le zoom...

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2016-10-20 17:16*

    FEAT: mémorisation des noeud fixé (coordonnée relative au centre)
    FIX: mise à jour du zoom en coordonnée relatives au centre

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2016-10-19 02:07*

    FEAT: [internal] ajout du package.json
    FEAT: noeud fixé au clique, relaché au double clique + retour visuel du survol.

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2016-10-18 10:41*

    FEAT: ajout d'un chargeur de config : par défaut de l'application, par défaut de la carte, réglages utilisateur
    FEAT: modularisation de la persistance des options utilisateur (ou des états de l'application)
    FEAT: affichage d'un disclamer pour IE
    FEAT: ajout de quelques test unitaire et de l'environnement nécessaire à la exécuter
    REFACTO: découpage en dossiers avec allData pour ce qui est spécifique à une carte et staticApp pour ce qui est général à l'application.
    DOC: ajout d'un README.md

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2016-10-16 20:35*

    FEAT: Zoom 100% fonctionnel
    WIP: zoom test (et ajustement de la vitesse de placement et stabilisation)

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2016-10-15 07:52*

    FIXME: trop de nouveauté et pas assez de refactoring.
    FEAT: Première version avec un graph affiché au milieu.
    FEAT: gestion du redimentionnement
    FEAT: gestion de l'espacement entre les neud
    FEAT: affichage de texte pour chaque noeud
    FEAT: affichage d'icone pour chaque noeud
    FEAT: compteur de fps

*👤 [1000i100](mailto:git@1000i100.fr) ⏰ 2016-10-13 07:33*

    FEAT: interface avec zone déployable
