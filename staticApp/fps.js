define(['./smartEvents'], (ev) => {
	'use strict';

	let fpsTicks, lissageFPS, fpsDisplayerNodes, decimalsDisplayed, fpsMax,fpsMin;

	function init(config){
		if(!config) return ev.need('config',init);
		fpsTicks = [];
		fpsMax = 0;
		fpsMin = 9999;
		if(config.fps.displayerId) fpsDisplayerNodes = {
			"cur":document.getElementById(config.fps.displayerId.cur),
			"max":document.getElementById(config.fps.displayerId.max),
			"min":document.getElementById(config.fps.displayerId.min)
		};
		lissageFPS = config.fps.sampleOn;
		decimalsDisplayed = config.fps.decimals;
		if (fpsDisplayerNodes && fpsDisplayerNodes.cur) setInterval(fpsRefresh, s2ms(config.fps.refreshEvery));
		ev.send('fps.ready');
		window.fps = {
			init,
			tick,
			calc,
			min,
			max,
			multiTimeout
		};
	}
	function tick() {
		fpsTicks.push(Date.now());
		if(fpsTicks.length>1){
			const currentFps = fpsTicks.length / (fpsTicks[fpsTicks.length-1] - fpsTicks[0]) * 1000;
			fpsMax = Math.max(fpsMax,currentFps);
			fpsMin = Math.min(fpsMin,currentFps);
		}
	}
	function calc() {
		const obsolete = Date.now() - (lissageFPS*1000);
		fpsTicks = fpsTicks.filter(function (t) {
			return t > obsolete;
		});
		return fpsTicks.length / lissageFPS;
	}
	function max() {
		return fpsMax;
	}
	function min() {
		return fpsMin;
	}
	function fpsRefresh() {
		if(fpsDisplayerNodes.cur)fpsDisplayerNodes.cur.innerText = round2Decimal(calc(),decimalsDisplayed);
		if(fpsDisplayerNodes.max)fpsDisplayerNodes.max.innerText = round2Decimal(max(),decimalsDisplayed);
		if(fpsDisplayerNodes.min)fpsDisplayerNodes.min.innerText = round2Decimal(min(),decimalsDisplayed);
	}
	function s2ms(s){
		return Math.floor(s*1000);
	}
	function round2Decimal(number,decimalNumber){
		return Math.round(Math.pow(10,decimalNumber)*number)/Math.pow(10,decimalNumber);
	}

	//TODO: mieux ranger multiTimeout dont le seul lien avec fps est le temps.
	function multiTimeout(frequence, duration, func) {
		for (let t = 0; t < duration; t += frequence) setTimeout(func, t);
	}

	init();
	return {
		init,
		tick,
		calc,
		min,
		max,
		multiTimeout
	};

});
