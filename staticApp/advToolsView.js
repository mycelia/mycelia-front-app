define([
	'./smartEvents',
	'./htmlTools',
	'js-yaml'
], (ev, htmlTools,jsyaml) => {
	const buildNode = htmlTools.buildNode;
	let toolsContainer;
	let config = {};
	function init() {
		ev.need('config',async (cfg)=>{
			config = cfg;
			toolsContainer = document.getElementById("advanced-tools");//config.legendAreaId);
			await renderAdvTools();
			ev.send('advToolsView.ready');
		});

	}
	async function ajax(url,data=undefined){
		return new Promise(
			function (resolve, reject) {
				let postData;
				const request = new XMLHttpRequest();
				request.onload = function () {
					if (this.status === 200) {
						// Success
						resolve(this.response);
					} else {
						// Something went wrong (404 etc.)
						reject(new Error(this.statusText));
					}
				};
				request.onerror = function () {
					reject(new Error(
						'XMLHttpRequest Error: '+this.statusText));
				};
				if(!data) request.open('GET', url,true);
				else{
					request.open('POST', url,true);
					postData = json2post(data);
				}
				request.crossDomain = true;
				request.withCredentials = true;
				request.send(postData);
			});
	}
	function json2post(json){
		console.log(json);
		const post = new FormData();
		for(let key in json){
			post.append(key,JSON.stringify(json[key]));
		}
		console.log(post);
		return post;
	}
	async function renderAdvTools(){
		if(config.persistanceAPI){
			const whatCanIDoUrl = config.persistanceAPI+'/auth/user-info';
			try{
				const access = JSON.parse(await ajax(whatCanIDoUrl));
				console.log(access);
				const logged = buildNode("#logged");
				logged.innerHTML = `Bienvenue ${access.user}. Vos droits : ${access.access.join(', ')} <a href="${config.persistanceAPI}/auth/logout" target="_blank">Déconnexion</a>`;
				toolsContainer.appendChild(logged);
				ev.give('logged',access);

				const save = buildNode("#saveButton.pictoButton");
				save.addEventListener('click',async ()=>{
					const graph = await ev.need('fullGraph');
					const tradContent = (await ev.need('tradLoader')).getTradData();
					const saveUrl = `${config.persistanceAPI}/commit/${graph.commitHash}/files`;
					try{
						const res = await ajax(saveUrl,{'files':{
							'data.yml':jsyaml.safeDump(JSON.parse(JSON.stringify(graph))),
							'public/lang/fr.yml':jsyaml.safeDump(JSON.parse(JSON.stringify(tradContent)))
						}});
						console.log(JSON.parse(res).text);
						if(JSON.parse(res).text === '200: saved') {
							localStorage.clear();
							alert("données et traductions sauvegardées.");
						}
						else {
							console.error("sauvegarde échouée : ",res);
							alert("sauvegarde échouée : "+JSON.parse(res).text);
						}
					} catch (e){
						console.error(e);
						alert("sauvegarde échouée : "+e);
					}
				});
				toolsContainer.appendChild(save);

			} catch (e){
				const login = buildNode("#loginButton.pictoButton");
				login.addEventListener('click',async ()=>{
					const email = prompt("Merci d'indiquer votre email pour recevoir votre lien d'authentification.", "mycelia-admin@yopmail.com");
					const authUrl = config.persistanceAPI+'/auth/email/'+email;
					const response = JSON.parse(await ajax(authUrl,{url:window.location.href}));
					if(response.text === "Token sent") alert("email d'authentificaiton envoyé");
					else alert(response.text);
				});
				toolsContainer.appendChild(login);
			}

			/*
			si authentifié : lister les droit, lien sauvegarder/publier, lien déconnecter
			sinon : s'authentifier
			 */
		}
	}

	return {
		init
	}
});
