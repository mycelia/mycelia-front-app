- [ ] J'ai effectué une recherche pour vérifier que je ne suis pas entrain de créer un doublon.
- [ ] Je pense que ce bug concerne mycelia-front-app (sinon postez le dans le projet concerné)

## Descriptif du problème :

**Comportement attendu :**

**Comportement constaté :**

## Procédure détaillée pour reproduire le problème :



## Infos technique :
Complétez ce que vous savez remplir, je vous demanderais le reste si nécessaire.

- Système d'exploitation (et sa version) : 
- Navigateur web (et sa version) : 
- version de mycelia-cli globale : (mycelia --version)
- version de mycelia-cli locale : (cf. package.json)
- version de mycelia-front-app : (cf. package.json)
- version de mycelia-server-nodejs : (cf. package.json)
